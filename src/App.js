import { useSelector, useDispatch } from "react-redux";

import { incNumberAction, decNumberAction, resetNumberAction } from "./actions";
import "./App.css";

function App() {
  const state = useSelector((state) => state.counterReducer);
  const dispatch = useDispatch();
  return (
    <div className="container">
      <h1>Increment/Decrement Counter</h1>
      <center>
        <input type="number" value={state} />
      </center>
      <center>
        <button
          className="btn btn-sm btn-outline-success"
          onClick={() => dispatch(incNumberAction(state))}
        >
          <b>Inc</b>
        </button>
        <button
          className="btn btn-sm btn-outline-secondary"
          onClick={() => {
            dispatch(resetNumberAction(state));
          }}
        >
          <b>Reset</b>
        </button>
        <button
          className="btn btn-sm btn-outline-success"
          onClick={() => dispatch(decNumberAction(state))}
        >
          <b>Dec</b>
        </button>
      </center>
    </div>
  );
}

export default App;
