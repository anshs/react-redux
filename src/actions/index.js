export const incNumberAction = () => {
  return {
    type: "INCREMENT",
  };
};

export const decNumberAction = () => {
  return {
    type: "DECREMENT",
  };
};

export const resetNumberAction = () => {
  return {
    type: "RESET",
  };
};
