import changeTheNumberReducer from "./incDec";
import { combineReducers } from "redux";

const rootReducer = combineReducers({
  counterReducer: changeTheNumberReducer,
});

export default rootReducer;
